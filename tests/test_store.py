# -*- coding: utf-8 -*-
import kvlib
import unittest


class TestStore(unittest.TestCase): pass


def make_test_exception(description, call, exc, *args, **kwargs):
    def test(self):
        self.assertRaises(exc, call, *args, **kwargs)
    test.__doc__ = description
    return test


test_args_data = {
    'put': {'key': '/test', 'value': 'test_val'},
    'get': {'key': '/test'},
    'delete': {'key': '/test'},
    'exists': {'key': '/test'},
    'watch': {'key': '/test'},
    'watch_tree': {'directory': '/test_dir'},
    'new_lock': {},
    'list': {'directory': '/test_dir'},
    'delete_tree': {'directory': '/test_dir'},
    'atomic_put': {},
    'atomic_delete': {},
    'close': {},
}
for method in kvlib.Store.__dict__.keys():
    if not method.endswith('_'*2) and not method.startswith('_'*2) and method in test_args_data:
        setattr(
            TestStore,
            'test_%s_exception' % method,
            make_test_exception(
                    'The %s method of the %s class should raise an exception: NotImplementedException' %
                    (method, kvlib.Store.__name__),
                    getattr(kvlib.Store, method),
                    kvlib.NotImplementedException,
                    kvlib.Store(),
                    **test_args_data[method]
            )
        )
