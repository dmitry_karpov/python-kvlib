"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='kvlib',
    version='0.0.1',
    description='libkv provides a library to store metadata',
    long_description=long_description,
    url='https://github.com/pypa/sampleproject',
    author='Dmitry K.',
    author_email='s--m--i--l--e@yandex.ru',
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    keywords='libkv',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=[
        'python-etcd==0.4.3',
        'python-consul==0.4.7',
        'requests',
    ],
    test_suite="tests",
    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
)