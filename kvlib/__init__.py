# -*- coding: utf-8 -*-

from .store import Store
from .result import Result
from . import etcd
from . import consul
import importlib


def get_backends():
    return KV_LIST


def new_store(backend, *args, **kwargs):
    try:
        i = importlib.import_module('.%s' % backend, 'kvlib')
    except ImportError:
        raise BackendNotSupportedException('Backend storage not supported yet, please choose one of')
    else:
        return i.Store(*args, **kwargs)

CONSUL = 'consul'
ETCD = 'etcd'

KV_LIST=[
    CONSUL,
    ETCD
]


class KVLibBaseException(BaseException):
    """
    Generic kvlib xception
    """
    def __init__(self, *args, **kwargs):
        BaseException.__init__(self, *args, **kwargs)


class NotImplementedException(KVLibBaseException):
    """
    NotImplementedException is thrown when the method not implemented in backend k/v store
    """
    pass


class BackendNotSupportedException(KVLibBaseException):
    """
    BackendNotSupportedException is thrown when the backend k/v store is not supported by libkv
    """
    pass


class CallNotSupportedException(KVLibBaseException):
    """
    CallNotSupportedException is thrown when a method is not implemented/supported by the current backend
    """
    pass


class NotReachableException(KVLibBaseException):
    """
    NotReachableException is thrown when the API cannot be reached for issuing common store operations
    """
    pass


class CannotLockException(KVLibBaseException):
    """
    CannotLockException is thrown when there is an error acquiring a lock on a key
    """
    pass


class KeyModifiedException(KVLibBaseException):
    """
    KeyModifiedException is thrown during an atomic operation if the index does not match the one in the store
    """
    pass


class KeyNotFoundException(KVLibBaseException):
    """
    KeyNotFoundException is thrown when the key is not found in the store during a Get operation
    """
    pass


class PreviousNotSpecifiedException(KVLibBaseException):
    """
    PreviousNotSpecifiedException is thrown when the previous value is not specified for an atomic operation
    """
    pass


class KeyExistsException(KVLibBaseException):
    """
    KeyExistsException is thrown when the previous value exists in the case of an atomic_put
    """
    pass
