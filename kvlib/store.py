# -*- coding: utf-8 -*-
import kvlib
__all__ = (
    'Store',
)


class Store(object):
    def put(self, key, value, ttl=None):
        """
        Put a value at the specified key
        :param key: Key
        :param value: Value
        :param ttl: Keys in kv store can be set to expire after a specified number of seconds.
        """
        raise kvlib.NotImplementedException

    def get(self, key):
        """
        Get a value given its key
        :param key: Key
        """
        raise kvlib.NotImplementedException

    def delete(self, key):
        """
        Delete the value at the specified key
        :param key: Key
        """
        raise kvlib.NotImplementedException

    def exists(self, key):
        """
        Verify if a Key exists in the store
        :param key: Key
        """
        raise kvlib.NotImplementedException

    def watch(self, key):
        """
        Watch for changes on a key
        :param key: Key
        """
        raise kvlib.NotImplementedException

    def watch_tree(self, directory):
        """
        WatchTree watches for changes on child nodes under
        a given directory
        """
        raise kvlib.NotImplementedException

    def new_lock(self):
        """
        NewLock creates a lock for a given key.
        The returned Locker is not held and must be acquired
        with `.Lock`. The Value is optional.
        """
        raise kvlib.NotImplementedException

    def list(self, directory):
        """
        List the content of a given prefix
        """
        raise kvlib.NotImplementedException

    def delete_tree(self, directory):
        """
        delete_tree deletes a range of keys under a given directory
        """
        raise kvlib.NotImplementedException

    def atomic_put(self):
        """
        Atomic CAS operation on a single value.
        Pass previous = nil to create a new key.
        """
        raise kvlib.NotImplementedException

    def atomic_delete(self):
        """
        Atomic delete of a single value
        """
        raise kvlib.NotImplementedException

    def close(self):
        """
        Close the store connection
        """
        raise kvlib.NotImplementedException
