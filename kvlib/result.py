# -*- coding: utf-8 -*-


class Result:
    _node_props = {
        'key': None,
        'value': None,
        'expiration': None,
        'ttl': None,
        'modifiedIndex': None,
        'createdIndex': None,
        'newKey': False,
        'dir': False,
    }

    def __init__(self):
        for attr in self._node_props:
            setattr(self, attr, self._node_props[attr])
