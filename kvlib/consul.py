# -*- coding: utf-8 -*-
import kvlib
import consul
__all__ = (
    'Store',
)


class Store(kvlib.Store):
    pass